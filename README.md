# Meteorological Conversions in Go

Performs various meteorological conversions.

## Reference

1. doc/Humidity_Conversion_Formulas_B210973EN-F.pdf  
https://www.vaisala.com/en/file/6221/download?token=KiF_ojau

# Vapor Density (function of air pressure and dew point temperature)

Water vapor density is computed by first calculating the vapor saturation pressure (hPa) at the dew point temperature (K). Vapor density in ppmv is then 10e6 * vapPres / (airPres - vapPres) where airPres is the total air pressure.

# Vapor Saturation Pressure (function of air temperature)

Water vapor saturation pressure is computed differently depending on if it is above water or ice. See section 2 of the referenced Vaisala formula document for both equations.
