// Package metcon performs meteorological conversions
package metcon

import (
	"fmt"
	"math"
)

// VaporDensityFromDewPt computes water vapor density from pressure in hPa and
// dew point temperature in K.
// Returns vapor density in ppmv (ppm by volume).
func VaporDensityFromDewPt(pressure, dewPoint float64) (float64, error) {
	if dewPoint <= 0 {
		return 0, fmt.Errorf("invalid dew point temperature: %f", dewPoint)
	}
	// The math simplifies.
	// RH = 100 * vap(airTemp) / satvap(airTemp)
	// RH = 100 * satvap(dewpt) / satvap(airTemp)
	// so
	// vap(airTemp) = satvap(dewpt)
	// Vapor pressure equals saturation vapor pressure of dew point
	vapPres := VaporSaturationPressure(dewPoint)
	if vapPres < 0 {
		return 0, fmt.Errorf("invalid vapor pressure: %f", vapPres)
	}
	// ppmv for dry air
	// ppmv = 10E6 * vapPres / (totalPres -vapPres)
	difference := pressure - vapPres
	if difference <= 0 {
		return 0, fmt.Errorf("vapor pressure %f must be greater than total pressure %f", vapPres, pressure)
	}
	return 1e6 * vapPres / difference, nil
}

// VaporSaturationPressure computes the vapor saturation pressure from
// the air temperature in K.
// Different formulae for computing over water or ice based on temperature.
// Returns vapor saturation pressure in hPa.
func VaporSaturationPressure(temperature float64) float64 {
	if temperature < 273.15 {
		return VSPOverIce(temperature)
	}
	return VSPOverWater(temperature)
}

// VSPOverWater computes vapor saturation pressure over water from temperature
// in K.
// Returns vapor saturation pressure in hPa.
func VSPOverWater(temperature float64) float64 {
	criticalTemp := 647.096
	criticalPres := 220640.
	c1 := -7.85951783
	c2 := 1.84408259
	c3 := -11.7866497
	c4 := 22.6807411
	c5 := -15.9618719
	c6 := 1.80122502

	theta := 1 - (temperature / criticalTemp)
	vsp := c1 * theta
	vsp += c2 * math.Pow(theta, 1.5)
	vsp += c3 * math.Pow(theta, 3)
	vsp += c4 * math.Pow(theta, 3.5)
	vsp += c5 * math.Pow(theta, 4)
	vsp += c6 * math.Pow(theta, 7.5)
	vsp *= criticalTemp / temperature
	vsp = math.Exp(vsp) * criticalPres

	return vsp
}

// VSPOverIce computes vapor saturation pressure over ice from temperature
// in K.
// Returns vapor saturation pressure in hPa
func VSPOverIce(temperature float64) float64 {
	triplePointTemp := 273.16
	triplePointVaporPres := 6.11657
	a0 := -13.928169
	a1 := 34.707823

	theta := temperature / triplePointTemp

	vsp := a0 * (1 - math.Pow(theta, -1.5))
	vsp += a1 * (1 - math.Pow(theta, -1.25))
	vsp = math.Exp(vsp) * triplePointVaporPres

	return vsp
}
