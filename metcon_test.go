package metcon

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestVaporDensityFromDewPtSuccess(t *testing.T) {
	tdp := 280.15
	p := 998.
	vp, err := VaporDensityFromDewPt(p, tdp)
	assert.Nil(t, err, "error not nil")
	assert.Equal(t, 10142, int(vp), "should be equal")
}

func TestVaporDensityFromDewPtError(t *testing.T) {
	tdp := 0.
	p := 998.
	_, err := VaporDensityFromDewPt(p, tdp)
	expected := fmt.Sprintf("invalid dew point temperature: %f", tdp)
	assert.EqualError(t, err, expected)
}
